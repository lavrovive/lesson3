'use strict'

window.onload = function () {
    //variables
    var txt = document.querySelector('#txt');
    var chck = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
    var btn = document.querySelector("#btn");
    var monkey = document.querySelector('#monkey');
    var money = document.querySelector('#money');
    var blue = document.querySelector('#blue');


    btn.addEventListener('click', MyApp);
    MyApp();
};

//valid
const Validate = (data) => {
    let isDataArray = Array.isArray(data)
    if( isDataArray ) {
        let chck = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
        return chck.test(txt.value);
    }
};

function MyApp() {

    monkey.innerHTML='';
    money.innerHTML='';
    blue.innerHTML='';

    var fetchedData = fetch(
        txt.value==="" ? "http://www.json-generator.com/api/json/get/cjEipSbdpe?indent=2" : txt.value
        ).then(function(response) {
            return response.json();
        }).then(data => {
            let validation = Validate(data);
            console.log('Validation:', validation);
            if( validation ){
                renderInterface(data);
            }
        }).catch(err => {
            console.log(err);
            return validation;
        });

    function renderInterface( data ){

        monkey.insertAdjacentHTML('afterBegin','<br>'+'<b>Monkeys:</b>'+'<br>');
        var research1 = (item) => { if (item.favoriteFruit=='banana') { monkey.insertAdjacentHTML('beforeEnd',item.name+'<br>') }; };
        data.forEach(research1);

        money.insertAdjacentHTML('afterBegin','<br>'+'<b>With money and diploma:</b>'+'<br>');
        var research2 = (item) => { if (item.age>25&&item.balance.replace(/[^-0-9-.]/gim,'')>2000.00) { money.insertAdjacentHTML('beforeEnd',item.name+'<br>') }; };
        data.forEach(research2);

        blue.insertAdjacentHTML('afterBegin','<br>'+'<b>Active blue-eyed women:</b>'+'<br>')
        var research3 = (item) => { if (item.eyeColor=='blue'&&item.gender=='female'&&item.isActive===true) { blue.insertAdjacentHTML('beforeEnd',item.name+'<br>') }; };
        data.forEach(research3);

    };
};
/*
txt.addEventListener('keyup', onlineValid);
function onlineValid() {
    var res = document.querySelector(".result");
    var myReq = new XMLHttpRequest();
    myReq.open('GET', txt.value);
    myReq.onload = function () {
        res.innerHTML = "GOOD VALID";
        res.classList.add("good");
        return;
        console.log(res.classList);
    };
    myReq.onerror = function () {
        res.classList.add("bad");
        res.innerHTML = "BAD VALID";

    };
    myReq.send();
};*/

//1
/*    function test() {
        fetch(txt.value).then(function (response) {
            return response.json();
        }).then(data => {
            console.log(Array.isArray(data));
        })
    };*/

//2
/*    function test() {
        fetch(txt.value).then(function (response) {
            return response.json();
        }).then(data => {
            try{
                console.log(Array.isArray(data));
    } catch(err){
            console.log(err);
        }
    })
    };*/

//3
/*    var test = function () {
        fetch(txt.value).then(function (response) {
            return response.json();
        }).then(data => {
            console.log(true);
            return Array.isArray(data);
        }).catch(err => {
            console.log(err);
            return false;
        });
    };*/


/*    //checking url
    function valid () {
        if (chck.test(txt.value)===true && test===true) {
            alert("Validation complite, now you can look for resoults");
            btn.removeEventListener('click', btn1);
            btn.addEventListener('click', MyApp);
        } else {
            alert("Not valid");
        };
    }*/
